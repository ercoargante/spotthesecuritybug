# README #

### What is this repository for? ###

This repository is about security code reviews. First a (manual) spot-the-security-bugs challenge will be done to train yourself in review code with a security prespective. Then a security analysis tool will be used to find bugs.

### How do I get set up? ###

* Clone or download https://bitbucket.org/ercoargante/spotthesecuritybug/ to your c:\ of your laptop. Reason for c:\ is that the repo already contains long path names, so if you put it under a long path, errors might occur. 
* In IntelliJ, choose "open" and select ~\spotthesecuritybug\spring-boot-samples\jsp\pom.xml; choose "open as project" and wait a while.


### Using the application ###

* This step is not really necessary, as this exercise is about code review.
* Download and install the Tomcat application server from https://tomcat.apache.org/download-80.cgi
* To verify correct installation, inside IntelliJ, right-click on src/main/webapp/WEB-INF/jsp/welcome.jsp and press the green run button.
* Open a browser window on http://localhost:8080 and a welcome message should appear.


### Security code review ###

* Only the file ~\spotthesecuritybug\spring-boot-samples\jsp\src\main\webapp\WEB-INF\jspspotTheBug.jsp needs to be reviewed for security bugs. The code contains at least 20 bugs. Of course we can never know for sure whether there are more!! One can also identify at least 20 recommendations/improvements.
* Basic application flow. The application contains a simple login mechanism. Logging in can be done by supplying a username, a password and a ‘nonce’. If the login is successful, a session token is generated with the nonce and the username. The username is appended to the session token. The session token is then stored in a cookie. The browser must always send the cookie and a nonce in the URL. To verify if the session is valid, the server recalculates the token with the username and the nonce. If the session token is valid, the user is greeted with his user data.
*  Structure of spotTheBug.jsp:
    * spotTheBug.jsp is a server-side template containing Java code. The code between `<%! .. %>` contains declarations. The code between `<% .. %>` is the code being executed. The code between `<%= .. >` is being evaluated and the result is interpreted as HTML.
    * So spotTheBug.jsp starts with declaring some methods; this is the part between `<%! .. %>`.
    * Then some Java code is executed; this is the part between `<% .. %>`. This code retrieves info from the HTTP request and performs some security logic.
    * Only now the HTML body is started. This part of the code looks ugly because it contains a Java if-statement and HTML statements intertwined.
    * `<%= data %>` shows information about the user, but only if authorization has been given. 

### Using security code analysis tools ###

It is interesting to see whether a tool can automatically spot bugs. We're going to use FindSecBugs. 
* Add the FindSecBugs plugin to IntelliJ by following: https://github.com/find-sec-bugs/find-sec-bugs/wiki/IntelliJ-Tutorial
* FindSecBugs ignores .jsp files (another very good reason to not put any logic in a template file!!). For this reason the Java code from the spotTheBug.jsp file has also been copied to the NotUsed.java class. This class is not used by the project, so it is dead code, but it allows testing FindSecBugs.
* Right-click on the project, go to FindBugs menu option, select "Analyze Project Files" and look at the bugs found by FindSecBugs.
* Compare the bugs found by FindSecBugs by the manually identified bugs.
* Alternatively SonarQube also performs some security checks.
